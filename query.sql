select o.descripcion as operativo,date_part('year',t.fecha_emision) as a�o,
date_part('month',t.fecha_emision) as mes,to_char(t.fecha_emision,'dd/mm/yyyy') as fecha,
c.descripcion as especialidad,pr.descripcion as producto,s.descripcion as tipprod,
to_char(now(),'HH:MI AM') as hora, sum(d.cantidad) as cantidad,sum(((d.monto - d.dscto)*d.cantidad*d.ind_neutro)) as total,fp.descripcion as formapago
from tickets t 
inner join operativo o on substring(t.nro_historia,1,3)=o.id_oper
inner join detalles d on t.nro_historia=d.nro_historia
left join consultorios c on t.id_consultorio=c.id_consultorio
inner join productos pr on d.id_producto=pr.id_producto 
left join tablatipo s on pr.tipo=s.id_tipo and s.id_tabla='5'
left join tablatipo fp on t.forpago=fp.id_tipo and fp.id_tabla='12'
where t.anulado<>'S' and t.fecha_emision>='D_FECHA' and t.fecha_emision<='D_FECHA'
group by o.descripcion,date_part('year',t.fecha_emision),date_part('month',t.fecha_emision),
to_char(t.fecha_emision,'dd/mm/yyyy'),c.descripcion,pr.descripcion,s.descripcion,fp.descripcion order by fp.descripcion,
cast(to_char(t.fecha_emision,'dd/mm/yyyy') as timestamp),5,6;
