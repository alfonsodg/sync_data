#!/bin/bash

place="VMT"
query_name=$(<query.sql)
today=`date +%Y-%m-%d`
dir_data="/opt/data_db"
destination_file=$dir_data"/"$place"-"$today".csv"
mkdir $dir_data
chmod 777 $dir_data
new_query="COPY ("${query_name//D_FECHA/$today}") TO '"$destination_file"' WITH CSV HEADER;"
psql -d villa_maria_del_triunfo -c "$new_query"
scp $destination usuario@sisol.gob.pe:/opt/data_db
